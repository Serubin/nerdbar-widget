refreshFrequency: false

render: (output) ->
  ""

style: """
  top: 0
  left: 0
  height: 22px
  width: 100%
  background-color: rgba(255,255,255,0.50)
  -webkit-backdrop-filter: blur(16px)
  z-index: -1
"""
